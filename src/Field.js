import React, { useState } from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import styled from "styled-components";
import { useSpring, animated } from "react-spring";
import NumberFormat from "react-number-format";

const WrapperStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const InputsWrapperStyled = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;

const InputStyled = styled.input`
  border-width: 0;
  margin: 0;
  padding: 0;
  font-size: 24px;
  background: transparent;
  outline: none;
  width: 100%;

  @media (prefers-color-scheme: dark) {
    color: rgb(187, 187, 187);
  }
`;

const InputNameStyled = styled(InputStyled)`
  font-weight: bold;
  transform-origin: 0% 10%;
`;

const InputOutcomeStyled = styled(InputStyled)`
  font-size: 30px;
  width: 100%;
  @media screen and (max-width: 768px) {
    font-size: 20px;
  }
`;

const RemoveButtonStyled = styled.button`
  border-width: 0;
  outline: none;
  background: #ffdbdb;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  margin-right: 10px;
  height: 50px;

  @media (prefers-color-scheme: dark) {
    color: #333;
  }
`;

const StatusPositiveStyled = styled.div`
  font-size: 40px;
  color: #78c378;
`;

const StatusNegativeStyled = styled.div`
  font-size: 40px;
  color: #bd5656;
`;

const LeftSideStyled = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  align-items: center;
`;

const Field = ({
  name,
  handleChangeName,
  handleChangeOutcome,
  handleRemove,
  outcome,
  needPay,
  needGive,
  className
}) => {
  const [isNameFocus, setNameFocus] = useState(false);
  const [isOutcomeFocus, setOutcomeFocus] = useState(false);

  const nameInputStyles = useSpring({
    transform: `scale(${isNameFocus ? 2.2 : 1})`,
    opacity: isOutcomeFocus ? 0 : 1
  });

  const outcomeInputStyles = useSpring({
    transformOrigin: "0% 100%",
    opacity: isNameFocus ? 0 : 1,
    transform: `scale(${isOutcomeFocus ? 2 : 1})`
  });

  const statusStyles = useSpring({
    opacity: isNameFocus || isOutcomeFocus ? 0 : 1
  });

  return (
    <WrapperStyled className={className}>
      <LeftSideStyled>
        <RemoveButtonStyled onClick={handleRemove}>
          <DeleteIcon />
        </RemoveButtonStyled>
        <InputsWrapperStyled>
          <InputNameStyled
            as={animated.input}
            style={nameInputStyles}
            value={name}
            onInput={({ value }) => handleChangeName(value)}
            onFocus={() => setNameFocus(true)}
            onBlur={() => setNameFocus(false)}
          />
          <animated.div style={outcomeInputStyles}>
            <NumberFormat
              customInput={InputOutcomeStyled}
              thousandSeparator={true}
              suffix={"₽"}
              onFocus={() => setOutcomeFocus(true)}
              onBlur={() => setOutcomeFocus(false)}
              onValueChange={({ value }) => handleChangeOutcome(value)}
              value={outcome}
            />
          </animated.div>
        </InputsWrapperStyled>
      </LeftSideStyled>
      <animated.div style={statusStyles}>
        {!!needGive && (
          <StatusPositiveStyled>
            ↑
            <NumberFormat
              value={needGive}
              suffix="₽"
              thousandSeparator={true}
              displayType={"text"}
            />
          </StatusPositiveStyled>
        )}
        {!!needPay && (
          <StatusNegativeStyled>
            ↓
            <NumberFormat
              value={needPay}
              suffix="₽"
              thousandSeparator={true}
              displayType={"text"}
            />
          </StatusNegativeStyled>
        )}
      </animated.div>
    </WrapperStyled>
  );
};

export default Field;

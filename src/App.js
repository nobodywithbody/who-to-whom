import React, { useReducer } from "react";
import {
  reducer,
  defaultState,
  REMOVE_GUY,
  ADD_GUY,
  CHANGE_OUTCOME,
  CHANGE_NAME
} from "./reducer";
import styled, { createGlobalStyle } from "styled-components";
import Field from "./Field";
import FloatButton from "./FloatButton";
import AddIcon from "@material-ui/icons/Add";
import { animated, useTransition } from "react-spring";
import NumberFormat from "react-number-format";

const ContainerStyled = styled.div`
  margin-top: 20vh;

  display: flex;
  max-width: 680px;
  margin: 0 auto;
  overflow-x: hidden;

  @media screen and (max-width: 768px) {
    margin-top: 5vh;
  }
`;

const TotalStyled = styled.h2`
  font-size: 2rem;
  text-align: center;
`;

const AddIconStyled = styled(AddIcon)`
  color: white;
`;

const FieldsWrapperStyled = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 100%;
`;

const FieldStyled = styled(Field)`
  margin-bottom: 25px;
`;

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
      "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: rgba(0, 0, 0, 0.87);
    box-sizing: border-box;

    @media (prefers-color-scheme: dark) {
      color: rgb(187, 187, 187);
      background: #1e1e1e;
    }
  }
`;

const App = () => {
  const [{ guys, total }, dispatch] = useReducer(reducer, defaultState);
  const transitions = useTransition(guys, guy => guy.id, {
    from: {
      opacity: 0,
      maxHeight: 100,
      transform: "translateX(-100px)"
    },
    enter: {
      opacity: 1,
      transform: "translateX(0px)"
    },
    leave: {
      opacity: 0,
      maxHeight: 0,
      transform: "translateX(100px)"
    }
  });

  return (
    <>
      <GlobalStyle />
      <ContainerStyled>
        <FieldsWrapperStyled>
          <TotalStyled>
            TOTAL:{" "}
            <NumberFormat
              value={total}
              suffix="₽"
              thousandSeparator={true}
              displayType={"text"}
            />
          </TotalStyled>
          {transitions.map(({ item, props, key }) => (
            <animated.div style={props} key={key}>
              <FieldStyled
                name={item.name}
                outcome={item.outcome}
                needPay={item.needPay}
                needGive={item.needGive}
                handleChangeName={name =>
                  dispatch({ type: CHANGE_NAME, name, id: item.id })
                }
                handleChangeOutcome={outcome =>
                  dispatch({ type: CHANGE_OUTCOME, outcome, id: item.id })
                }
                handleRemove={() => dispatch({ type: REMOVE_GUY, id: item.id })}
              />
            </animated.div>
          ))}
        </FieldsWrapperStyled>
        <FloatButton onClick={() => dispatch({ type: ADD_GUY })}>
          <AddIconStyled />
        </FloatButton>
      </ContainerStyled>
    </>
  );
};

export default App;

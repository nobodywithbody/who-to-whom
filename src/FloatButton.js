import styled from "styled-components";

export default styled.button`
  background-color: rgb(245, 0, 87);
  right: 16px;
  bottom: 32px;
  position: fixed;
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
    0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12);
  width: 56px;
  height: 56px;
  font-weight: 500;
  border-radius: 50%;
  padding: 0;
  margin: 0;
  &:focus,
  &:active {
    outline: none;
  }
`;

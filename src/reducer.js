export const CHANGE_OUTCOME = "CHANGE_OUTCOME";
export const ADD_GUY = "ADD_GUY";
export const REMOVE_GUY = "REMOVE_GUY";
export const CHANGE_NAME = "CHANGE_NAME";

let id = 1;

const makeGuy = ({
  outcome = 0,
  name = "smbd " + id,
  needPay = 0,
  needGive = 0
} = {}) => ({
  outcome,
  name,
  needPay,
  needGive,
  id: ++id
});

export const defaultState = {
  guys: [makeGuy()],
  total: 0
};

const getTotalOutcome = guys =>
  guys.reduce((acc, { outcome }) => acc + parseInt(outcome), 0);

const calcNeedPay = (guys, total) => {
  const eachNeedPay = Math.round(total / guys.length);

  return guys.map(guy => ({
    ...guy,
    needGive: guy.outcome > eachNeedPay && guy.outcome - eachNeedPay,
    needPay: guy.outcome < eachNeedPay && eachNeedPay - guy.outcome
  }));
};

const changeName = (guys, name, id) =>
  guys.reduce((acc, cur) => {
    if (cur.id === id) {
      return [
        ...acc,
        {
          ...cur,
          name
        }
      ];
    }

    return [...acc, cur];
  }, []);

const changeOutcome = (guys, id, outcome) =>
  guys.reduce((acc, cur) => {
    if (cur.id === id) {
      return [
        ...acc,
        {
          ...cur,
          outcome
        }
      ];
    }

    return [...acc, cur];
  }, []);

export const reducer = (state, action) => {
  switch (action.type) {
    case CHANGE_OUTCOME:
      const guys = changeOutcome(state.guys, action.id, action.outcome);
      const total = getTotalOutcome(guys);

      return {
        ...state,
        guys: calcNeedPay(guys, total),
        total
      };
    case CHANGE_NAME: {
      return {
        ...state,
        guys: changeName(state.guys, action.name, action.id)
      };
    }
    case ADD_GUY: {
      const guys = [...state.guys, makeGuy()];
      return {
        ...state,
        guys: calcNeedPay(guys, state.total)
      };
    }

    case REMOVE_GUY: {
      const newGuys = state.guys.filter(item => item.id !== action.id);
      const total = getTotalOutcome(newGuys);

      return {
        ...state,
        guys: calcNeedPay(newGuys, total),
        total
      };
    }
    default:
      return state;
  }
};
